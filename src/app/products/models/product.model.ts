export interface Product {
  id?: number;
  description?: string;
  price?: number;
  available?: Date | string;
  stock?: number;
  rating?: number;
}
